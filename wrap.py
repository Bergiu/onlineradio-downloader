#!/usr/bin/python3
import urllib.request
import os
import sys

def getWebsite(link):
	html = urllib.request.urlopen(link).read()
	text=str(html)
	return text

def splitContentBetween(var,start,end):
	rows1=var.split(start)
	rows2=rows1[len(rows1)-1].split(end)
	del(rows1[0])
	rows=rows1
	rows.append(rows2[0])
	return rows

def removeDuplicates(liste):
	liste.sort()
	liste=list(set(liste))
	liste.sort()
	return liste

def findFirstYvideo(url):
	youtube=getWebsite(url)
	test=splitContentBetween(youtube,'<a href="/watch?','"')
	print("Checking links:")
	for i in range(len(test)):
		param=test[i].split('"')[0]
		print(param)
		paramlist=param.split("v=")
		if len(paramlist) > 1:
			param=paramlist[1]
			print(param)
			if not "&amp;list=" in param:
				break
		else:
			param=""
	return param


surfmusik="http://www.surfmusik.de/playlist/"
#playlist="sunshinelive.php" #hier
playlist=sys.argv[1]
text=getWebsite(surfmusik+playlist)
songrows=splitContentBetween(text,'<tr width="100%" bgcolor="#000080" onMouseOver=this.style.backgroundColor="0033CC" onMouseOut=this.style.backgroundColor="#000080">',"</tr>")
songlist=[]

for row in songrows:
	var=splitContentBetween(row,'<font face="Arial"  size="2" color="#FFFFFF">','</font>')
	songname=var[2]
	songlist.append(songname)

songlist=removeDuplicates(songlist)
for song in songlist:
  print(song)

urls=[]
vurls=[]
os.popen('echo "" > %s.links'%(playlist))
for song in songlist:
	print("Search: "+song)
	url="https://www.youtube.com/results?"+urllib.parse.urlencode({"search_query":song})
	urls.append(url)
	print(url)
	param=findFirstYvideo(url)
	if not param == "":
		vurl="https://www.youtube.com/watch?"+urllib.parse.urlencode({"v":param})
		vurls.append(vurl)
		print("Found a video:")
		print(vurl+"\n")
		os.popen('echo "%s" >> %s.links'%(vurl,playlist))
	else:
		print("Can't find a video :(")


