# onlineradio-downloader
Downloads the last 100 songs from http://www.surfmusik.de/playlist/ "your-playlist"

## How to:

1. Download onlineradio-downloader.
2. Install youtube-dl.
3. Goto Google and find a playlist from http://www.surfmusik.de/playlist/. For Example http://www.surfmusik.de/playlist/sunshinelive.php.
4. Make sure you are allowed to download the songs from the playlist.
5. Start download_playlist.sh "your-playlist"  

  > ./download_playlist.sh sunshinelive.php

6. Wait until it is finished.
